//Sirine Aoudj 1935903
package client;

//This class contains all the users information
public class user {
	private int currentMoney = 100;

	//This returns the user current money
	public int getCurrentMoney() {
		return this.currentMoney;
	}
	
	//this method returns a string that will be displayed depending on what you choose and the amount of money
	public String placeBet(int bet, int moneyBet) {
		int head = 1;
		int tail = 2;
		String result = "";
		
		//gets a random number to define heads or tails
		int headOrtail = (Math.random() <= 0.5) ? 1 : 2;
		
		//if user bet corresponds to the random then add the amount bet to the current money
		// and set the string to you won this amount of money and the bet placed
		if(bet == headOrtail) {
			this.currentMoney += moneyBet;
			if(bet == head) {
				result= "You chose head and won " + moneyBet + "$";
			}
			else {
				result= "You chose tail and won " + moneyBet + "$";
			}
		}
		// else removes the amount bet on the current amount and display how much you lost
		else {
			this.currentMoney -= moneyBet;
			result= "You lost " + moneyBet + "$";
		}
		return result;
	}
	
	//this verifies if the user can or cannot place a bet depending on their current amount of money
	public Boolean eligibleForBet(int moneyBet) {
		if(this.currentMoney - moneyBet >= 0) {
			return true;
		}
		else {
			return false;
		}
	}
}
