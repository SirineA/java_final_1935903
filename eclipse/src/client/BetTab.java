//Sirine Aoudj 1935903
package client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class BetTab extends Tab{
	//create all the elements we need on our bet tab
	//user that plays the game
	user client;
	//the buttons to place a bet
	Button tail = new Button("Tail");
	Button head = new Button("Head");
	
	//the amount of money bet
	TextField moneyBet = new TextField();
	//All the information of the user
	Label currentMoneyLabel = new Label("Current amount of Money: ");
	Label currentMoney = new Label("");
	Label messageToUser = new Label("");
	int countBet = 0;
	Label numOfBet = new Label("");
	
	//tab constructor
	public BetTab(user player) {
		super("Play!");
		this.client = player;
		this.setContent(addContent());
	}
	
	//add a gridpane to the tab that contains the elements of the bet tab
	public GridPane addContent() {
		
		GridPane gridPane = new GridPane();
		
		gridPane.add(moneyBet, 1, 1);
		gridPane.add(tail, 2, 1);
		gridPane.add(head, 3, 1);
		gridPane.add(currentMoneyLabel, 1, 2);
		gridPane.add(currentMoney, 2, 2);
		gridPane.add(messageToUser, 1, 5);
		gridPane.add(numOfBet, 1, 4);
		
		gridPane.setHgap(20);
		gridPane.setVgap(20);
		
		//Add the event on the buttons
		this.tail = addSubmitEvent(tail, moneyBet, messageToUser);
		this.head = addSubmitEvent(head, moneyBet, messageToUser);
		
		return gridPane;
	}
	
	//This method get a button and will check which button it is and then place a bet
	private Button addSubmitEvent(Button currentButton, TextField moneyBet, Label messageToUser) {
		currentButton.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent e) {
				//first it check if the bet amount field is filled if not it displays you cannot place a bet
				if(checkField()) {
					int bet = Integer.parseInt(moneyBet.getText());
					
					//Then it check if the user has the money to place that bet
					if(client.eligibleForBet(bet)) {
						
						//then if the button is a tail button then it places a tail bet with the bet amount
						if(currentButton.getText().equals("Tail")) {
							messageToUser.setText(client.placeBet(2,bet));
						}
						//else it will do it but with a head button
						else {
							messageToUser.setText(client.placeBet(1,bet));
						}
						//This allows us to keep track of amount of bet placed to refresh the current money
						countBet += 1;
						numOfBet.setText(String.valueOf(countBet));
					}
					else {
					    messageToUser.setText("Not enough money...");
							
						}
					}
				}
		});
		return currentButton;
	}

	//This check if the textfield is empty or not
	public boolean checkField() {
		boolean containFields = true;
		if(moneyBet.getText().trim().isEmpty()
			) {
			containFields = false;
			messageToUser.setText("Need to input a bet!");
		}
		return containFields;
	}
	
	//this updates the current amount of money
	public void refresh() {
		currentMoney.setText(client.getCurrentMoney()+"$");
	}

}
