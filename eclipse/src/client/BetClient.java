//Sirine Aoudj 1935903
package client;


import org.omg.CORBA.portable.ApplicationException;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class BetClient extends Application{

	public void start(Stage stage) throws ClassNotFoundException {
		user userInst = new user();
		
		//set the stage size
		final int WIDTH = 800;
		final int HEIGHT = 500;
		Group root = new Group();
		
		Scene scene = new Scene(root,WIDTH,HEIGHT);
		scene.setFill(Color.WHITE);

		//add the bet tab to the tab pane
		TabPane tb = new TabPane();
		BetTab betTab = new BetTab(userInst);
		
		//this checks the amount of bet text in the bet tab and if it changes it refreshes the current amount of money
		betTab.numOfBet.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
					betTab.refresh();
			}
		});

		
		tb.getTabs().add(betTab);

		
		root.getChildren().add(tb);
		
		stage.setScene(scene);
		stage.show();

	}
	/**
	 * main launches the application
	 * @param args
	 */
	public static void main(String[]args) {
		Application.launch(args);
	}

	

}
