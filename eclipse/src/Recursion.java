//Sirine Aoudj 1935903
package client;

public class Recursion {

	public static int recursiveCount(int[]numbers, int n) {
		int count=0;

		 if(numbers.length > 0)
		        {int value=numbers[0];
		    int a[]= new int [numbers.length-1];
		     
		    if(n<value && value%2 !=0)
		        count=1;
		 
		    for(int i=1;i<numbers.length;i++){
		        a[i-1]=numbers[i];
		    }
		 
		    return count+recursiveCount(a,n);
		}
		 
		 else
		     return 0;
		 
		}
	public static void main(String[] arg) {
		int[] numbers = new int[] {21,22,23,24,25,26};
		int t = recursiveCount(numbers,20);
		System.out.println(t);
	}
}
